A continuación se detallan los archivos o carpetas definidos en el ejercicio:

-Proyecto (Adjunto HelloWorld):
	Para la realización de este ejercicio, he usado un proyecto con JAVA, Maven y Spring Boot.
	El archivo resultante, es un .war.


-Docker (Adjunto Dockerfile):
	
	La imagen de docker que he usado es un tomcat donde el war generado en la carpeta del proyecto "target" se copia en el path /srv/tomcat/webapps/ del tomcat.


-Kubernetes (Adjunto deployment.yml y service.yml):

	Para desplegar en Kubernetes he usado un objeto de tipo deployment con 2 replicas y que utiliza la imagen de docker generada a partir del Dockerfile anterior.
	Tambíen he creado un objeto de tipo service que apunta al objeto de tipo deployment.


-Jenkinsfile Pipeline (Adjunto Jenkinsfile):

	El Jenkinsfile contiene 8 stages:
		* GIT Info: obtiene la información de GIT y del repositorio de Nexus según la rama de Git que estemos usando para pasar el ciclo de CI/CD.
		* Build: construye el proyecto usando maven. Se pasa el parametro -DskipTests ya que los tests se realizan en el siguiente stage.
		* Test: realiza los tests de Junit del proyecto. Tests unitarios.
		* SonarQube: realiza un análisis del código del proyecto según los parametros definidos (en esta practica no se han definido parametros ya que no se ha configurado SonarQube).
		* Quality Gate: espera a que se complete el análisis de SonarQube y devuelve el estado del "quality gate".
		* Release Nexus repository: suebe la nueva release del proyecto al Nexus Repository.
		* Docker image: genera la imagen de docker para poder usarla en el despliegue de Kubernetes.
		* Deploy kubernetes: despliega el proyecto en Kubernetes mediante los yml explicados anteriormente.
	

	Estas stages las podemos agrupar en 3 bloques:
		* Continuous Integration: en este bloque encontrariamos dos subgrupos: Build, que estarian los stages de GIT info y Build; Test, que estarían los stages de Test, SonarQube y QualityGate.
		
		* Continuous Delivery: la stage que pertenece a este bloque es Release Nexus repository.
		
		* Continuous Deployment: las dos stages de este bloque son Docker image y Deploy kubernetes.

-Resultados (Adjunto img1.png, img2.png, img3.png, img4.png y consoleText):
	
	Por último, he adjuntado varios archivos he imagenes para mostrar el resultado obtenido.
	La imagen img1.png y ek archivo consoleText muestra el output de la consola al construir.
	Las imagenes img2.png e img3.png muestra el resultado del Stage y que se ha pasado correctamente ya que está todo en verde y el status en azul.
	Para finalizar, la imagen img4.png muestra el resultado de Blue Ocean, un plugin instalado des de Jenkins.
	


NOTA: para no harcodear las variables en el Jenkinsfile se debería usar parametros del Jenkins y Helm para los archivos de Kubernetes.